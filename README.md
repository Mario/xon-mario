# Evidence supporting my claims against Mario

### Note this is very incomplete, I have a lot more instances, will add when time allows, and also some PMs but have to ask for permission to share.

My claims:
- He knowingly and intentionally falsely accused me of distributing cheats in public several times
- He tried gaslighting me about it
- He has repetitively shown signs of other manipulative behavior over a long timeframe (confirmed by LH)
- He has selectively slowed down contributions by me (TODO examples)

Things i can't prove:
- He asked or incited musicgoat to insult me and spread lies about me on servers, then added him to credits as a reward (TODO example logs)

My impression is he has been trying to demotivate me from working on xon and is trying to get rid of me. Possible explanations:
- He considers me a more competent dev and feels his position is threatened (TODO examples where he clearly doesn't understand basic programming concepts)
- He wants to stop the port to rust (again, might feel threatened if he's incapable of learning the lang)
- He wants people to see xon as his game (consistent with LH's theory he's a covert narcissist)
    - e.g. he keeps calling it his game - "thank you for a playing a my game a" TODO more examples

See the txt files:
    - [logs-freenode-xonotic.txt](logs-freenode-xonotic.txt)
    - [logs-qnet-xonotic.txt](logs-qnet-xonotic.txt)

- My comments are marked with `@@@`, no need to read all of it, mostly just the parts before my notes. The rest is there for context.
- The logs are from Solid's bouncer, he didn't want them to be made completely public though.
- I am not even sure they're complete, the timestamps are a bit weird sometimes (e.g. 2019-09-23 for 2 days) and i can't find a couple conversations i clearly recall.

Meanwhile, Mario is trying to kick me from the team for supposedly slandering him and the team. Moro sent mario a screenshot of said "slander" - see [Screenshot_20210308_010451.png](Screenshot_20210308_010451.png) - I stand behind everything i said there and i also don't see how this is in any way "unacceptable" as div said - everything there is true and this is not what i told others, i just asked questions.

I am ok with being kicked from the team later (for things i said 2+ years ago) but
- I do note nobody made a big deal out of them until it became convenient
- Mario has behaved in worse ways, though he was more subtle about it
- Mario's behavior is what caused me to talk to other people about it in the first place - it was not slander, I didn't even tell details to most people, just asked questions - now he is trying to spin it as slandering him and the team to "key figures in the community" and "a long line of community members" - in fact i talked to about 15 people, mostly who have quit xon and asked if they've seen manipulative behavior.
